package ru.amster.tm.exception.user;

import ru.amster.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("ERROR! Access denied...");
    }

}