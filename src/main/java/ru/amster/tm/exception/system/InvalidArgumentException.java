package ru.amster.tm.exception.system;

import ru.amster.tm.exception.AbstractException;

public class InvalidArgumentException extends AbstractException {

    public InvalidArgumentException() {
        super("ERROR! Invalid argument...");
    }

}