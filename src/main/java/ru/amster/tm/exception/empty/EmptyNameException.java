package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}