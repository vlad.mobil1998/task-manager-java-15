package ru.amster.tm.bootstrap;

import ru.amster.tm.api.servise.ITerminalService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.exception.system.InvalidArgumentException;
import ru.amster.tm.exception.system.InvalidCommandException;
import ru.amster.tm.role.Role;
import ru.amster.tm.service.TerminalService;
import ru.amster.tm.util.TerminalUtil;

public class Bootstrap {

    ITerminalService terminalService = new TerminalService();

    private void initUsers() {
        terminalService.getUserService().create("test", "test", "test@test.ru");
        terminalService.getUserService().create("admin", "admin", Role.ADMIN);
    }

    public final void run(final String[] args) {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCmd(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseCmd(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = terminalService.getCommand(cmd);
        if (command == null) throw new InvalidCommandException();
        command.execute();
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        for (String arg : args) {
            System.out.println(" ");
            final AbstractCommand argument = terminalService.getArgument(arg);
            if (argument == null) throw new InvalidArgumentException();
            argument.execute();
        }
        return true;
    }

}