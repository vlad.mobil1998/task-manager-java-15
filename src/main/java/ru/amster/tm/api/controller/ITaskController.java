package ru.amster.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTask();

    void createTask();

    void showTaskById();

    void showTaskByName();

    void showTaskByIndex();

    void updateTaskByIndex();

    void updateTaskById();

    void removeTaskByName();

    void removeTaskByIndex();

    void removeTaskById();

}