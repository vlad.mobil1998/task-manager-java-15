package ru.amster.tm.api.servise;

public interface ServiceLocator {

    IAuthService getAuthService();

    IUserService getUserService();

    ITaskService getTaskService();

    IProjectService getProjectService();

}