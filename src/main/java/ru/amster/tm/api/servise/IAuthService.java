package ru.amster.tm.api.servise;

public interface IAuthService {

    String getUserId();

    void login(String login, String password);

    void logout();

    void registration(String login, String password, String email);
}