package ru.amster.tm.api.servise;

import ru.amster.tm.command.AbstractCommand;

import java.util.Collection;

public interface ITerminalService extends ServiceLocator {

    void registry(final AbstractCommand command);

    void registry(final AbstractCommand command, final AbstractCommand argument);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommand(final String cmd);

    AbstractCommand getArgument(final String arg);

    @Override
    IAuthService getAuthService();

    @Override
    IUserService getUserService();

    @Override
    ITaskService getTaskService();

    @Override
    IProjectService getProjectService();

}