package ru.amster.tm.command.auth;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class Login extends AbstractCommand {

    LoginCheck loginCheck;

    public Login(LoginCheck loginCheck) {
        this.loginCheck = loginCheck;
    }

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Sign in system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        if (loginCheck.getLoginCheck()) throw new AccessDeniedException();
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        serviceLocator.getAuthService().login(login, password);
        loginCheck.setLoginCheck(true);
        System.out.println("[OK]");
    }

}