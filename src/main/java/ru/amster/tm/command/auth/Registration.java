package ru.amster.tm.command.auth;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class Registration extends AbstractCommand {

    LoginCheck loginCheck;

    public Registration(LoginCheck loginCheck) {
        this.loginCheck = loginCheck;
    }

    @Override
    public String name() {
        return "reg";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Register now";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRATION]");
        if (loginCheck.getLoginCheck()) throw new AccessDeniedException();
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        System.out.println("[ENTER EMAIL]");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registration(login, password, email);
        System.out.println("[OK]");
    }

}