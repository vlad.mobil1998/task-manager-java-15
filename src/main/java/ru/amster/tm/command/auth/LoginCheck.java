package ru.amster.tm.command.auth;

public class LoginCheck {

    private boolean loginCheck = false;

    public boolean getLoginCheck() {
        return loginCheck;
    }

    public void setLoginCheck(boolean loginCheck) {
        this.loginCheck = loginCheck;
    }

}