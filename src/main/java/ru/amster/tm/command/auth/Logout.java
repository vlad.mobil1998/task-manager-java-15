package ru.amster.tm.command.auth;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.exception.user.AccessDeniedException;

public class Logout extends AbstractCommand {

    LoginCheck loginCheck;

    public Logout(LoginCheck loginCheck) {
        this.loginCheck = loginCheck;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Log out system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        if (!loginCheck.getLoginCheck()) throw new AccessDeniedException();
        serviceLocator.getAuthService().logout();
        loginCheck.setLoginCheck(false);
        System.out.println("[OK]");
    }

}