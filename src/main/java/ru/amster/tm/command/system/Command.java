package ru.amster.tm.command.system;

import ru.amster.tm.api.servise.ITerminalService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.service.TerminalService;

import java.util.Collection;

public class Command extends AbstractCommand {

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Display terminal command";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        ITerminalService terminalService = new TerminalService();
        final Collection<AbstractCommand> commands = terminalService.getCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command.name());
        }
    }

}