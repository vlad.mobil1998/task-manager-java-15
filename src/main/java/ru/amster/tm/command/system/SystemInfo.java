package ru.amster.tm.command.system;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.util.NumberUtil;

public class SystemInfo extends AbstractCommand {

    @Override
    public String name() {
        return "info";
    }

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String description() {
        return " - Display information about system.";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");

        final int availableProcessor = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessor);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

}