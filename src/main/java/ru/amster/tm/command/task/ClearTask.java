package ru.amster.tm.command.task;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.exception.user.AccessDeniedException;

public class ClearTask extends AbstractCommand {

    @Override
    public String name() {
        return "task-clr";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Remove all task";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
    }

}