package ru.amster.tm.command.task;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TaskUtil;

import java.util.List;

public class ShowTasks extends AbstractCommand {

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASK]");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        for (Task task : tasks) TaskUtil.showTask(task);
        System.out.println("[OK]");
    }

}