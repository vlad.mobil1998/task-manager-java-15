package ru.amster.tm.command.task;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class UpdateTaskById extends AbstractCommand {

    @Override
    public String name() {
        return "task-upd-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Update task by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = serviceLocator.getTaskService().findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = serviceLocator.getTaskService().updateTaskById(userId, id, name, description);
        if (taskUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}