package ru.amster.tm.command.task;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class RemoveTaskById extends AbstractCommand {

    @Override
    public String name() {
        return "task-re-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = serviceLocator.getTaskService().removeOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            throw new EmptyTaskException();
        } else System.out.println("[OK]");
    }

}