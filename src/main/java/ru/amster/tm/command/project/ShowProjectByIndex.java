package ru.amster.tm.command.project;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;
import ru.amster.tm.util.TerminalUtil;

public class ShowProjectByIndex extends AbstractCommand {

    @Override
    public String name() {
        return "project-v-i";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Show project by index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Integer maxIndex = serviceLocator.getProjectService().numberOfAllProjects(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

}