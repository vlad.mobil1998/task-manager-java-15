package ru.amster.tm.command.project;

import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class UpdateProjectByIndex extends AbstractCommand {

    @Override
    public String name() {
        return "project-upd-i";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Update project by index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Integer maxIndex = serviceLocator.getProjectService().numberOfAllProjects(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) return;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        final Project projectUpdate = projectService.updateProjectByIndex(userId, index, name, description);
        if (projectUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}