package ru.amster.tm.command.project;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;
import ru.amster.tm.util.TerminalUtil;

public class ShowProjectByName extends AbstractCommand {

    @Override
    public String name() {
        return "project-v-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Show project by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = serviceLocator.getProjectService().findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

}