package ru.amster.tm.command.project;

import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.exception.user.AccessDeniedException;

public class ClearProject extends AbstractCommand {

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " - Remove all project";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
    }

}