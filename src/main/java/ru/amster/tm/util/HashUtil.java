package ru.amster.tm.util;

import ru.amster.tm.exception.empty.EmptyValueException;

public interface HashUtil {

    String SECRET = "12sasd213";

    Integer ITERATOR = 32768;


    static String salt(final String value) {
        if (value == null) throw new EmptyValueException();
        String result = value;
        for (int i = 0; i < ITERATOR; i++)
            result = md5(SECRET + result + SECRET);
        return result;
    }

    static String md5(final String value) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}