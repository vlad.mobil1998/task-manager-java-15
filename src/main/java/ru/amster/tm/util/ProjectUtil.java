package ru.amster.tm.util;

import ru.amster.tm.exception.empty.EmptyProjectException;

public interface ProjectUtil {

    static void showProject(final ru.amster.tm.entity.Project project) {
        if (project == null) throw new EmptyProjectException();
        System.out.println("ID: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

}
