package ru.amster.tm.repository;

import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyLoginException;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) throw new EmptyIdException();
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) throw new EmptyLoginException();
        return removeUser(user);
    }

}