package ru.amster.tm.service;

import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.*;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.command.auth.Login;
import ru.amster.tm.command.auth.LoginCheck;
import ru.amster.tm.command.auth.Logout;
import ru.amster.tm.command.auth.Registration;
import ru.amster.tm.command.project.*;
import ru.amster.tm.command.system.*;
import ru.amster.tm.command.task.*;
import ru.amster.tm.repository.ProjectRepository;
import ru.amster.tm.repository.TaskRepository;
import ru.amster.tm.repository.UserRepository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TerminalService implements ITerminalService {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final LoginCheck loginCheck = new LoginCheck();

    {
        registry(new Help(), new Help());
        registry(new About(), new About());
        registry(new SystemInfo(), new SystemInfo());
        registry(new Version(), new Version());
        registry(new Command());
        registry(new Argument());
        registry(new Login(loginCheck));
        registry(new Logout(loginCheck));
        registry(new Registration(loginCheck));
        registry(new CreateProject());
        registry(new ShowProjectByIndex());
        registry(new ShowProjectByName());
        registry(new ShowProjectById());
        registry(new ShowProjects());
        registry(new UpdateProjectById());
        registry(new UpdateProjectByIndex());
        registry(new RemoveProjectById());
        registry(new RemoveProjectByName());
        registry(new RemoveProjectByIndex());
        registry(new ClearProject());
        registry(new CreateTask());
        registry(new ShowTaskByIndex());
        registry(new ShowTaskByName());
        registry(new ShowTaskById());
        registry(new ShowTasks());
        registry(new UpdateTaskById());
        registry(new UpdateTaskByIndex());
        registry(new RemoveTaskById());
        registry(new RemoveTaskByName());
        registry(new RemoveTaskByIndex());
        registry(new ClearTask());
        registry(new Exit());
    }

    public void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    public void registry(final AbstractCommand command, final AbstractCommand argument) {
        if (command == null) return;
        command.setServiceLocator(this);
        argument.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(argument.arg(), argument);
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public AbstractCommand getCommand(final String cmd) {
        return commands.get(cmd);
    }

    @Override
    public AbstractCommand getArgument(final String arg) {
        return arguments.get(arg);
    }

}