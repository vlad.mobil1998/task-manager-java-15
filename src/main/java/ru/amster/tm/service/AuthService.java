package ru.amster.tm.service;

import ru.amster.tm.api.servise.IAuthService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId = null;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registration(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

}